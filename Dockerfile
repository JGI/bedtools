FROM alpine:3.5

ENV BEDTOOLS_VERSION=2.26.0

RUN set -ex \
&&  apk upgrade --update \
&&  apk add --update libstdc++ \
    curl \
    ca-certificates \ 
    bash \
    wget \
    tar \
    gzip \
    gcc \
    g++ \
    make \
    python \
    zlib-dev

ENV LIBRARY_PATH=/lib:/usr/lib

RUN mkdir /bedtools/

# downloading bedtools...
RUN cd /bedtools/ \
&& wget https://github.com/arq5x/bedtools2/archive/v${BEDTOOLS_VERSION}.tar.gz

# unpacking bedtools...
RUN cd /bedtools/ \
&& tar xvfz v${BEDTOOLS_VERSION}.tar.gz

# compiling...
# bedtools:
RUN cd /bedtools/bedtools2-${BEDTOOLS_VERSION}/ \
&& make \
&& make prefix=/bedtools/bedtools/ install
ENV PATH=/bedtools/bedtools/bin:$PATH

CMD ["/bin/bash"]
